// var http = require('http');
// var server = http.createServer(function(req, res) {
//     res.writeHead(200);
//     res.end('Hello World')
// }).listen(process.env.PORT || 3000);


let express = require('express');
let app = express();
let mongoose = require('mongoose');
let morgan = require('morgan');
let bodyParser = require('body-parser');
//let port = 8080;
let book = require('./app/routes/book');
//googleapi
// let googleapi = require('./app/routes/googleapi');

let config = require('config'); //we load the db location from the JSON files
//db options
let options = { 
				server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
                replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } 
              }; 

//db connection      
mongoose.connect(config.DBHost, options);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

//don't show the log when it is test
if(config.util.getEnv('NODE_ENV') !== 'test') {
	//use morgan to log at command line
	app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

//parse application/json and look for raw text                                        
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

app.get("/", (req, res) => res.json({message: "Hey! Welcome to Bookstore API!  Path eg. https://heroku2-vtc.herokuapp.com/book or https://heroku2-kelvintsang.c9users.io/book"}));

app.route("/book")
	.get(book.getBooks)
	.post(book.postBook);
app.route("/book/:id")
	.get(book.getBook)
	.delete(book.deleteBook)
	.put(book.updateBook);
	
// app.route("/updateDB")
// 	.post(googleapi.getBooks);

// app.set( 'port', ( process.env.PORT || 30000 ));
// // Start node server
// app.listen( app.get( 'port' ), function() {
//   console.log( 'Node server is running on port ' + app.get( 'port' ));
//   });
  
  
  app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});

   
 //app.listen(port);
 //console.log("Listening on port " + port);

module.exports = app; // for testing


